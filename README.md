# IceDog.RabbitMQ

## 介绍

RMQ的练习

## 注意

练习只有在一定场景下才是有意义的，否则没有任何意义

RabbitMQ消息模型的核心思想就是，生产者不把消息直接发送给队列。

实际上，生产者在很多情况下都不知道消息是否会被发送到一个队列中。
取而代之的是，生产者将消息发送到交换区。交换区是一个非常简单的东西，
它一端接受生产者的消息，另一端将他们推送到队列中。

交换区必须要明确的指导如何处理它接受到的消息。是放到一个队列中，还是放到多个队列中，
亦或是被丢弃。这些规则可以通过交换区的类型来定义。

可用的交换区类型有：direct，topic，headers，fanout。

Exchange：用于接收消息生产者发送的消息，有三种类型的exchange：direct, fanout,topic，不同类型实现了不同的路由算法；

RoutingKey：是RabbitMQ实现路由分发到各个队列的规则，并结合Binging提供于Exchange使用将消息推送入队列；

Queue：是消息队列，可以根据需要定义多个队列，设置队列的属性，比如：消息移除、消息缓存、回调机制等设置，实现与Consumer通信；


## 场景定义

RMQ的消息生成端不复杂，只有生成交换器，定义消息发布的key即可。剩下的消息匹配模式是客户端定义的队列来定义的

### hi-news

一个叫 hi-news 的通知系统，简写 HN ，就是通过RMQ产生消息，发送消息。详情查看项目里面的readme.md

### hi-broadcast

一个叫 hi-broadcast 的通知系统，简写 HB ，就是通过RMQ产生消息，发送消息。详情查看项目里面的readme.md

### hi-docs

对模型的序列化传输然后反序列化

## 参考文章

- https://blog.csdn.net/WuLex/article/details/52093448
- https://blog.csdn.net/WuLex/article/details/62043912

## 生成启动脚本，放在生成后

```
echo @echo off >$(TargetDir)/run.bat
echo dotnet $(ProjectName).dll >> $(TargetDir)/run.bat

```

## 总结

在rmq自定义服务端，定义一个connection，通过链接创建一个channel,
然后通过channel定义一个exchange，定义发送消息对应的routingKey

通过channel发送消息到rmq服务。

```c#
channel.BasicPublish(exchangeName, routeKey, properties, msgBytes);
```

在rmq 自定义客户端，定义一个connection，通过链接创建一个channel,
通过 channel 定义queue，然后从queue中读取消息进行处理。

```c#
channel.QueueBind(queueName, exchangeName, bindingKey);

using (var subscription = new Subscription(channel, queueName, false))
{
    Console.WriteLine("等待消息...");
    var encoding = new UTF8Encoding();
    while (channel.IsOpen)
    {
        BasicDeliverEventArgs eventArgs;
        var success = subscription.Next(2000, out eventArgs);
        if (!success) { continue; }
        var msgBytes = eventArgs.Body;
        var message = encoding.GetString(msgBytes);
        Console.WriteLine($"{uid}:{message}");
        channel.BasicAck(eventArgs.DeliveryTag, false);
    }
}
```