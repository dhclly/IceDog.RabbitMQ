﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceDog.HiDocs.Models
{
    public enum DocumentType
    {
        //日志
        Journal = 1,
        //论文
        Thesis = 2,
        //会议文件
        Meeting = 3
    }
}
