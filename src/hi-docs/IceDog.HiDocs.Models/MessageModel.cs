﻿using System;

namespace IceDog.HiDocs.Models
{
    public class MessageModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public DocumentType DocType { get; set; }
        public string Content { get; set; }
        public override string ToString()
        {
            return Title;
        }
    }
}
