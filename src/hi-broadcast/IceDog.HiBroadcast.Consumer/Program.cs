﻿using IceDog.RabbitMQ.ConfigurationService.Factories;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Text;

namespace IceDog.HiBroadcast.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            BeforeStart();
            RunMainLoop();
        }
        private static void BeforeStart()
        {
            Console.WriteLine("Client is starting");
        }
        private static void RunMainLoop()
        {
            ReceiveTextMessage();
        }
        /// <summary>
        /// 接收文本消息
        /// </summary>
        private static void ReceiveTextMessage()
        {
            try
            {
                var connectionFactory = FactoryProvider.GetRMQFactory();
                using (var connection = connectionFactory.CreateConnection("hibroadcast.text.cutsomer.conntection"))
                {
                    var exchangeName = "hibroadcast.text.exchange";
                    using (var channel = connection.CreateModel())
                    {
                        // 这指示通道不预取超过1个消息
                        channel.BasicQos(0, 1, false);
                        var uid = Guid.NewGuid().ToString("N").Substring(0,6);
                        //创建一个新的，持久的队列
                        var queueName = $"hibroadcast.text.queue.{uid}";
                        channel.QueueDeclare(queue:queueName,durable:false, exclusive:true,autoDelete:true,arguments: null);
                        //绑定队列到交换区
                        var bindingKey = "hibroadcast.text.routeKey";
                        channel.QueueBind(queueName, exchangeName, bindingKey);

                        using (var subscription = new Subscription(channel, queueName, false))
                        {
                            Console.WriteLine("等待消息...");
                            var encoding = new UTF8Encoding();
                            while (channel.IsOpen)
                            {
                                BasicDeliverEventArgs eventArgs;
                                var success = subscription.Next(2000, out eventArgs);
                                if (!success) { continue; }
                                var msgBytes = eventArgs.Body;
                                var message = encoding.GetString(msgBytes);
                                Console.WriteLine($"{uid}:{message}");
                                channel.BasicAck(eventArgs.DeliveryTag, false);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("发生了错误：" + ex);
                throw;
            }
        }
    }
}
