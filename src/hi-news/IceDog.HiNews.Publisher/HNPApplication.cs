﻿using IceDog.RabbitMQ.ConfigurationService.Factories;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IceDog.HiNews.Publisher
{
    public static class HNPApplication
    {
        public static void Run()
        {
            BeforeStart();
            RunMainLoop();
        }
        private static void BeforeStart()
        {
            Console.Clear();
            Console.WriteLine("Server start");
            for (int i = 3; i > 0; i--)
            {
                Console.WriteLine(i);
                Thread.Sleep(1000);
            }
            Console.WriteLine("Server is starting");
        }
        private static void RunMainLoop()
        {
            DatetimePublish();
        }

        /// <summary>
        /// 日期推送
        /// </summary>
        private static void DatetimePublish()
        {
            try
            {
                var connectionFactory = FactoryProvider.GetRMQFactory();
                var connection = connectionFactory.CreateConnection("hinews.dt.publisher.connection");
                var encoding = new UTF8Encoding();
                //频道名
                var exchangeName = "hinews.dt.exchange";

                var channel = connection.CreateModel();
                channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, true, false, null);

                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = 2; //消息是持久的，存在并不会受服务器重启影响 

                //推送普通消息
                Task.Factory.StartNew(() =>
                {
                    //准备开始推送
                    //发布的消息可以是任何一个(可以被序列化的)字节数组，如序列化对象，一个实体的ID，或只是一个字符串

                    while (true)
                    {
                        Thread.Sleep(3000);//3s推送一条消息
                        var msg = GetMessage();
                        var msgBytes = encoding.GetBytes(msg);
                        var routeKey = "hinews.dt.common.routeKey";//推送路由
                        channel.BasicPublish(exchangeName, routeKey, properties, msgBytes);
                    }
                });

                //推送VIP频道
                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(5000);//5s推送一条消息
                        var msg = GetMessage(true);
                        var msgBytes = encoding.GetBytes(msg);
                        var routeKey = "hinews.dt.vip.routeKey";//推送路由
                        channel.BasicPublish(exchangeName, routeKey, properties, msgBytes);
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("发生了错误：" + ex);
                throw;
            }
        }
        private static string GetMessage(bool isVIP = false)
        {
            if (isVIP) { return GetVIPDatetimeMessage(); }
            return GetDatetimeMessage();
        }
        private static string GetDatetimeMessage()
        {
            return $"【Common】尊敬的用户你好，当前时间是{DateTime.Now.ToLocalTime().ToLongTimeString()}";
        }
        private static string GetVIPDatetimeMessage()
        {
            return $"【VIP】尊敬的VIP用户你好，当前时间是{DateTime.Now.ToLocalTime().ToLongTimeString()}";
        }
    }
}
