﻿namespace IceDog.HiNews.Customer
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isVIP = false;
            if (args.Length > 0)
            {
                isVIP = args[0] == "vip";
            }
            CmdArgs.IsVIP = isVIP;
            HNCApplication.Run();
        }
    }
}
