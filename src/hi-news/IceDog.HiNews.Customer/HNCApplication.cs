﻿using IceDog.RabbitMQ.ConfigurationService.Factories;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Text;

namespace IceDog.HiNews.Customer
{
    public static class HNCApplication
    {
        public static void Run()
        {
            BeforeStart();
            RunMainLoop();
        }
        private static void BeforeStart()
        {
            Console.WriteLine("Client is starting");
        }
        private static void RunMainLoop()
        {
            if (CmdArgs.IsVIP)
            {
                ReceiveVIPDatetimeMessage();
            }
            else
            {
                ReceiveDatetimeMessage();
            }
            
        }
        
        /// <summary>
        /// 接收日期消息
        /// </summary>
        private static void ReceiveDatetimeMessage()
        {
            try
            {
                //频道名
                var exchangeName = "hinews.dt.exchange";
                var connectionFactory = FactoryProvider.GetRMQFactory();
                using (var connection = connectionFactory.CreateConnection("hinews.dt.cutsomer.conntection"))
                {
                    using (var channel = connection.CreateModel())
                    {
                        // 这指示通道不预取超过1个消息
                        channel.BasicQos(0, 1, false);
                        
                        //创建一个新的，持久的队列
                        var queueName = "hinews.dt.common.queue";
                        channel.QueueDeclare(queueName, true, false, false, null);
                        //绑定队列到交换区
                        var bindingKey = "hinews.dt.common.routeKey";
                        channel.QueueBind(queueName, exchangeName, bindingKey);

                        using (var subscription = new Subscription(channel, queueName, false))
                        {
                            Console.WriteLine("等待消息...");
                            var encoding = new UTF8Encoding();
                            while (channel.IsOpen)
                            {
                                BasicDeliverEventArgs eventArgs;
                                var success = subscription.Next(2000, out eventArgs);
                                if (!success) { continue; }
                                var msgBytes = eventArgs.Body;
                                var message = encoding.GetString(msgBytes);
                                Console.WriteLine(message);
                                channel.BasicAck(eventArgs.DeliveryTag, false);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("发生了错误：" + ex);
                throw;
            }
        }
        private static void ReceiveVIPDatetimeMessage()
        {
            try
            {
                //频道名
                var exchangeName = "hinews.dt.exchange";
                var connectionFactory = FactoryProvider.GetRMQFactory();
                using (var connection = connectionFactory.CreateConnection("hinews.dt.cutsomer.conntection"))
                {
                    using (var channel = connection.CreateModel())
                    {
                        // 这指示通道不预取超过1个消息
                        channel.BasicQos(0, 1, false);
                        
                        //创建一个新的，持久的队列
                        var queueName = "hinews.dt.vip.queue";
                        channel.QueueDeclare(queueName, true, false, false, null);
                        //绑定队列到交换区
                        var bindingKey = "hinews.dt.*.routeKey";
                        channel.QueueBind(queueName, exchangeName, bindingKey);
                        using (var subscription = new Subscription(channel, queueName, false))
                        {
                            Console.WriteLine("尊敬的VIP客户，请等待消息...");
                            var encoding = new UTF8Encoding();
                            while (channel.IsOpen)
                            {
                                BasicDeliverEventArgs eventArgs;
                                var success = subscription.Next(2000, out eventArgs);
                                if (!success) { continue; }
                                var msgBytes = eventArgs.Body;
                                var message = encoding.GetString(msgBytes);
                                var oldColor = Console.ForegroundColor;
                                if (message.IndexOf("VIP") > -1)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                }
                                Console.WriteLine(message);
                                if (message.IndexOf("VIP") > -1)
                                {
                                    Console.ForegroundColor = oldColor;
                                }
                                channel.BasicAck(eventArgs.DeliveryTag, false);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("发生了错误：" + ex);
                throw;
            }
        }
    }
}
