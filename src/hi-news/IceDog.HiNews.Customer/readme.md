﻿# Read Me

HiNews.Publisher 产生的消息的消费者

消费者分为两种，一种是普通消息消费者，另一种是VIP消息消费者。

启动普通消息消费者 run.bat

```
dotnet IceDog.HiNews.Customer.dll 
```

启动VIP消息消费者 run-vip.bat

```
dotnet IceDog.HiNews.Customer.dll vip
```

采用的topic 模式进行模糊匹配消息

普通通道消费者只接收普通消息

vip通道接收普通消息和vip消息，vip消息红色显示