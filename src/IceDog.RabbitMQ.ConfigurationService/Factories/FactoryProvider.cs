﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceDog.RabbitMQ.ConfigurationService.Factories
{
    public static class FactoryProvider
    {
        private static Dictionary<string, object> cache = new Dictionary<string, object>();
        /// <summary>
        /// 获取RabbitMQ工厂实例
        /// </summary>
        /// <returns></returns>
        public static ConnectionFactory GetRMQFactory()
        {
            var key = "rmq";
            if (cache.ContainsKey(key))
            {
                return cache[key] as ConnectionFactory;
            }
            //建立RabbitMQ连接和通道
            var connectionFactory = new ConnectionFactory
            {
                HostName = "127.0.0.1",
                Port = 5672,
                UserName = "admin",
                Password = "admin",
                AmqpUriSslProtocols = System.Security.Authentication.SslProtocols.None,
                AutomaticRecoveryEnabled = true, //自动重连
                RequestedFrameMax = uint.MaxValue,
                RequestedHeartbeat = ushort.MaxValue, //心跳超时时间
            };
            cache[key] = connectionFactory;
            return connectionFactory;
        }
        /// <summary>
        /// 获取Hi Docs 的 RabbitMQ工厂实例
        /// </summary>
        /// <returns></returns>
        public static ConnectionFactory GetDocRMQFactory()
        {
            var key = "rmq-hi-docs";
            if (cache.ContainsKey(key))
            {
                return cache[key] as ConnectionFactory;
            }
            //建立RabbitMQ连接和通道
            var connectionFactory = new ConnectionFactory
            {
                HostName = "127.0.0.1",
                Port = 5672,
                UserName = "admin",
                Password = "admin",
                AmqpUriSslProtocols = System.Security.Authentication.SslProtocols.None,
                AutomaticRecoveryEnabled = true, //自动重连
                RequestedFrameMax = uint.MaxValue,
                RequestedHeartbeat = ushort.MaxValue, //心跳超时时间
                TopologyRecoveryEnabled = true,
                VirtualHost = "/"//"/HiDocs"
            };
            cache[key] = connectionFactory;
            return connectionFactory;
        }
    }
}
